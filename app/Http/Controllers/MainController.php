<?php


namespace App\Http\Controllers;


use SimpleXLSX;
use const ARRAY_FILTER_USE_BOTH;

class MainController extends Controller
{
    public function index()
    {
        $errors = [];
        $filePath = base_path() . '/files/test.xlsx';
        if (file_exists($filePath)) {
            $xlsx = SimpleXLSX::parse($filePath);
            $rows = $xlsx->rows();
            $formData = [];
            foreach ($rows[0] as $key => $row) {
                if (preg_match('/^Filter:.+/', $row)) {
                    $formData[] = array_filter(array_column($rows, $key), function ($value) {
                        return !empty($value);
                    });
                }
            }

            $formStructure = [];
            foreach ($formData as $selectData) {
                if (count($selectData) > 1) {
                    $formStructure[] = [
                        'label' => str_replace('Filter:', '', $selectData[0]),
                        'items' => array_slice($selectData, 1),
                    ];
                }
            }
        } else {
            $formStructure = [];
            $errors = [
                'File ' . $filePath . ' does not exist',
            ];
        }

        return response()->view('welcome', [
            'formData' => json_encode([
                'structure' => $formStructure,
                'errors' => $errors,
            ]),
        ]);
    }

    public function download()
    {
        $filePath = base_path() . '/files/sample.pdf';
        return response()->file($filePath);
    }
}
