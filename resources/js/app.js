import Vue from 'vue';
import FormComponent from "./FormComponent";
import Vuex from 'vuex';
import vSelect from 'vue-select';

Vue.use(Vuex);
Vue.component('v-select', vSelect);

const store = new Vuex.Store({
    state: {
        formData: formData,
    },
    getters: {
        formData: state => {
            return state.formData;
        },
    },
});

const app = new Vue({
    store,
    el: '#vue-app',
    components: {
        'form_component': FormComponent
    },
    template: '<form_component></form_component>'
});

